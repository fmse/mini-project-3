package de.luh.se.fmse.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.viewers.AbstractZoomableViewer;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.viewers.IGraphEntityRelationshipContentProvider;
import org.eclipse.zest.core.viewers.ISelfStyleProvider;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;

import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

public class FirstLTSView extends ViewPart implements IZoomableWorkbenchPart {

	Collection<State> automata = new ArrayList<State>();

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}
	
	private GraphViewer viewer;


	/*
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */
	public class ViewContentProvider{
				
		public Collection<State> getNodes() {
			return FirstLTSView.this.automata;
		}
	}
	
	public class SimulationTraceContentProvider extends ArrayContentProvider
			implements IGraphEntityRelationshipContentProvider {

		@Override
		public Object[] getRelationships(Object source, Object destination) {
			List<Transition> rels = new ArrayList<Transition>();
			if(source instanceof State && destination instanceof State){
				State src = (State) source;
				State dest = (State) destination;
				for(Transition t : src.getOutgoingTransitions()){
					if(t.getToState() != null && t.getToState().equals(dest)){
						rels.add(t);
					}
				}
			}
			return rels.toArray();
		}
	}
	
	public class SimulationTraceLabelProvider extends LabelProvider implements ISelfStyleProvider{
				
		@Override
		public String getText(Object element) {
			if (element instanceof State) {
				State state = (State) element;
				String stateName = "";
				stateName += state.getName();
				for(String ap : state.getAtomicPrep()){
					stateName += "\n" + ap;
				}
				return stateName;
			}
			if (element instanceof Transition) {
				Transition transition = (Transition) element;
				return transition.getName();
			}
			throw new RuntimeException("Wrong type: "
					+ element.getClass().toString());
		}
		
		

		@Override
		public void selfStyleConnection(Object element,
				GraphConnection connection) {

			if (element instanceof Transition) {
				Transition t = (Transition) element;
				if (true) {
					connection.setLineStyle(Graphics.LINE_DASH);
				} else {
					connection.setLineStyle(Graphics.LINE_SOLID);
				}
				connection.setLineWidth(2);
			}
		}

		@Override
		public void selfStyleNode(Object element, GraphNode node) {
			if(element instanceof State){
				State state = (State) element;
				if((boolean) ((LTS) state.eContainer()).getStartStates().get(0).equals(state)){
					node.setBackgroundColor(new Color(Display.getCurrent(), 0, 149, 0));
				}else{
					node.setBackgroundColor(new Color(Display.getCurrent(), 220, 220, 220));
				}
				String tooltip = "";
				for(String formulas : state.getSatisfiedNodes()){
					tooltip += formulas + "\n";
				}
				node.setTooltip(new Label(tooltip));
			}
			
		}
	

	}



	/**
	 * This is a callback that create the viewer and initialize
	 * it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		viewer = new GraphViewer(parent, SWT.NONE);
		viewer.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
		viewer.setContentProvider(new SimulationTraceContentProvider());
		viewer.setLabelProvider(new SimulationTraceLabelProvider());
		ViewContentProvider model = new ViewContentProvider();
		viewer.setInput(model.getNodes());
		LayoutAlgorithm layout = setLayout(1);
		viewer.setLayoutAlgorithm(layout, true);
		viewer.applyLayout();

	}

	@Override
	public AbstractZoomableViewer getZoomableViewer() {
		return viewer;
	}


	private class ScenarioToolsSpringLayoutAlgorithm extends SpringLayoutAlgorithm {
		public ScenarioToolsSpringLayoutAlgorithm(int s) {
			super(s);
		}

		/**
		 * Minimum distance considered between nodes
		 */
		protected static final double MIN_DISTANCE = 10.0d;
	}

	private LayoutAlgorithm setLayout(int layoutNum) {
		LayoutAlgorithm layout;
		switch (layoutNum) {
			case 0:	layout = new ScenarioToolsSpringLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					((ScenarioToolsSpringLayoutAlgorithm) layout).setRandom(false);
			//		((SpringLayoutAlgorithm) layout).setSpringLength(1);
			//		((SpringLayoutAlgorithm) layout).setSpringMove(1);
			//		((SpringLayoutAlgorithm) layout).setSpringStrain(1.0d);
			//		((SpringLayoutAlgorithm) layout).setBounds(1000, 1000, 1000, 1000);
					break;	
			case 1:	layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
			case 2: layout = new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
			default:layout = new TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
					break;
		}
		
		// layout = new GridLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);// many intersections
		// layout = new HorizontalTreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);// not so good

		return layout;

	}
	
	private String getLayoutName(int layoutNum) {
		String layout;
		switch (layoutNum) {
			case 0:	layout = "Spring Layout";
					break;
			case 1:	layout = "Tree Layout";
					break;
			case 2: layout = "Radial Layout";
					break;
			default:layout = "Tree Layout";
					break;
		}
		return layout;
	}
	
	protected int currentLayout = 1;
	protected final int numLayouts = 3;
	
	protected void changeLayout(){
		currentLayout++;
		getViewer().setLayoutAlgorithm(setLayout(currentLayout % numLayouts));
		getViewer().applyLayout();
	}


	protected GraphViewer getViewer() {
		return viewer;
	}

	public void setInput(Collection<State> states){
		
		getViewer().setInput(states);
		getViewer().refresh();
			
		
	}
	
	@SuppressWarnings("unchecked")
	protected Collection<State> getInput(){
		if (getViewer().getInput() instanceof Collection<?>)
			return (Collection<State>) getViewer().getInput();
		else return (Collection<State>) new HashSet<State>();
	}

	protected void refreshCurrentModelMessageEventsList() {
		setInput((Collection<State>) this.automata);
	}


}