package de.luh.se.fmse.core.ba.composition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;
import de.luh.se.fmse.core.ParallelComposition;

public class BAComposition {
	public LTS performBAComposition(LTS lts1, LTS lts2){
		
		LTS composedBA = ParallelComposition.performParallelComposition(lts1, lts2);
		Map<State, List<State>> combinesStates = ParallelComposition.getNewStateCombinesStates();
		
//		EcoreUtil.Copier copier = new EcoreUtil.Copier();
		Copier copier = new Copier();
		LTS composedBACopy1 = (LTS) copier.copy(composedBA);
		copier.copyReferences();
		LTS composedBACopy2 = (LTS) copier.copy(composedBA);
		copier.copyReferences();
		
		for(State s : composedBA.getStates()){
			s.setName(s.getName() + ", 0");
		}
		for(State s : composedBACopy1.getStates()){
			s.setName(s.getName() + ", 1");
		}
		for(State s : composedBACopy2.getStates()){
			s.setName(s.getName() + ", 2");
		}
		List<Integer> indexFinalStatesCopy0 = new ArrayList<Integer>();
		List<Integer> indexFinalStatesCopy1 = new ArrayList<Integer>();
		
		for(int i=0; i < composedBA.getStates().size(); i++){
			List<State> states = combinesStates.get(composedBA.getStates().get(i));
			
			if(lts1.getFinalState().contains(states.get(0))
					|| lts1.getFinalState().contains(states.get(1))){
				indexFinalStatesCopy0.add(i);
			}
			if(lts2.getFinalState().contains(states.get(0))
					|| lts2.getFinalState().contains(states.get(1))){
				indexFinalStatesCopy1.add(i);
			}
		}
		
		for(Integer index : indexFinalStatesCopy0){
			State actualState = composedBA.getStates().get(index);
			List<Transition> toRemove = new ArrayList<Transition>();
			for(Transition t : actualState.getIncomingTransitions()){
				t.setToState(composedBACopy1.getStates().get(index));
				composedBACopy1.getStates().get(index).getIncomingTransitions().add(t);
				toRemove.add(t);
			}
			actualState.getIncomingTransitions().removeAll(toRemove);
		}

		for(Integer index : indexFinalStatesCopy1){
			State actualState = composedBACopy1.getStates().get(index);
			List<Transition> toRemove = new ArrayList<Transition>();
			for(Transition t : actualState.getIncomingTransitions()){
				if(composedBACopy1.getStates().contains(t.getFromState())){
					t.setToState(composedBACopy2.getStates().get(index));
					composedBACopy2.getStates().get(index).getIncomingTransitions().add(t);
					toRemove.add(t);
				}
			}
			actualState.getIncomingTransitions().removeAll(toRemove);
		}
		
		for(int index = 0; index < composedBACopy2.getStates().size(); index++){
			State actualState = composedBACopy2.getStates().get(index);
			
			composedBA.getFinalState().add(actualState);
			for(Transition t : actualState.getOutgoingTransitions()){
				t.getToState().getIncomingTransitions().remove(t);
				int indexToState = composedBACopy2.getStates().indexOf(t.getToState());
				t.setToState(composedBA.getStates().get(indexToState));
				composedBA.getStates().get(indexToState).getIncomingTransitions().add(t);
			}
		}
		
		composedBA.getStates().addAll(composedBACopy1.getStates());
		composedBA.getStates().addAll(composedBACopy2.getStates());
		
		boolean deletedSomething = true;
		while(deletedSomething){
			List<State> toRemoveStates = new ArrayList<State>();
			for(State state : composedBA.getStates()){
				if(composedBA.getStartStates().contains(state)){
					continue;
				}
				if(state.getIncomingTransitions().size() == 0){
					toRemoveStates.add(state);
					for(Transition t : state.getOutgoingTransitions()){
						t.getToState().getIncomingTransitions().remove(t);
					}
				}
					
			}
			composedBA.getStates().removeAll(toRemoveStates);
			if(toRemoveStates.size() == 0){
				deletedSomething = false;
			}
		}
		
		return composedBA;
	}
}
