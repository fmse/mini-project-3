package de.luh.se.fmse.core.helper;

import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

public class NestedDFSHelper {
	
	private LTS lts;
	private boolean circleFound = false;

	public void performNestedDFS(LTS lts) {
		if (lts == null)
			return;
		
		this.lts = lts;
		this.circleFound = false;
		
		for (State state : lts.getStartStates()) {
			this.dfsBlue(state);
		}
	}
	
	public boolean hasCircle() {
		return this.circleFound;
	}
	
	private void dfsBlue(State state) {
		state.setSatisfiedBlue(true);
		for (Transition successor : state.getOutgoingTransitions()) {
			if (!successor.getToState().isSatisfiedBlue())
				this.dfsBlue(successor.getToState());
		}
		
		if (lts.getFinalState().contains(state))
			this.dfsRed(state, state);
	}
	
	private void dfsRed(State state, State seed) {
		state.setSatisfiedRed(true);
		for (Transition successor : state.getOutgoingTransitions()) {
			if (!successor.getToState().isSatisfiedRed())
				this.dfsRed(successor.getToState(), seed);
			else
				if (successor.getToState().equals(seed)) {
					this.circleFound = true;
				}
		}
	}
}
