package de.luh.se.fmse.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import de.luh.se.fmse.lts.Alphabet;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.LtsFactory;
import de.luh.se.fmse.lts.State;
import de.luh.se.fmse.lts.Transition;

public class ParallelComposition {

	private static Map<State, List<State>> newStateCombinesStates;
	
	public static Map<State, List<State>> getNewStateCombinesStates() {
		return newStateCombinesStates;
	}

	public static LTS performParallelComposition(LTS lts1, LTS lts2) {

		//dfs components
		Stack<State> open = new Stack<State>();
		Map<String, State> stateNameToStateMap = new HashMap<String, State>(); //closed
		
		// new LTS
		LTS composition = LtsFactory.eINSTANCE.createLTS();
		
		newStateCombinesStates = new HashMap<State, List<State>>();
		
		// alphabet
		Alphabet newAlphabet = LtsFactory.eINSTANCE.createAlphabet();
		newAlphabet.getElements().addAll(lts1.getAlphabet().getElements());
		newAlphabet.getElements().addAll(lts2.getAlphabet().getElements());
		composition.setAlphabet(newAlphabet);

		// Startstate
		State startState = LtsFactory.eINSTANCE.createState();
		composition.getStates().add(startState);
		composition.getStartStates().add(startState);
		
		open.push(startState);
		
		// Name
		String startStateName = lts1.getStartStates().get(0).getName() + ", "
				+ lts2.getStartStates().get(0).getName();
		startState.setName(startStateName);
		stateNameToStateMap.put(startStateName, startState);
		
		List<State> combinedStates = new ArrayList<State>();
		combinedStates.add(lts1.getStartStates().get(0));
		combinedStates.add(lts2.getStartStates().get(0));
		newStateCombinesStates.put(startState, combinedStates);
		
		//Start
		while(!open.isEmpty()){
			State sourceCombinedState = open.pop();
			//build all next states
			State firstContainingState = newStateCombinesStates.get(sourceCombinedState).get(0);
			State secondContainingState = newStateCombinesStates.get(sourceCombinedState).get(1);
			
			for(Transition t : firstContainingState.getOutgoingTransitions()){
				if(lts2.getAlphabet().hasElement(t.getName())){
					//common event
					State nextState = LtsFactory.eINSTANCE.createState();
					State firstContainingTargetState = LtsFactory.eINSTANCE.createState();
					State secondContainingTargetState = null;
					Transition nextTransition = LtsFactory.eINSTANCE.createTransition();
					
					//look up if the other LTS contains the actual transition 
					f1: for(Transition t2 : secondContainingState.getOutgoingTransitions()){
						if(t2.getName().equals(t.getName())){
							secondContainingTargetState = t2.getToState();
							break f1;
						}
					}
					//other contained state doesn't support this transition
					if(secondContainingTargetState == null){
						continue;
					}
					
					nextTransition.setName(t.getName());
					nextTransition.setFromState(sourceCombinedState);
					composition.getTransitions().add(nextTransition);
					firstContainingTargetState = t.getToState();
					
					String nextStateName = firstContainingTargetState.getName() + ", " + secondContainingTargetState.getName();
					//if state is already explored
					if(stateNameToStateMap.containsKey(nextStateName)){
						nextTransition.setToState(stateNameToStateMap.get(nextStateName));
						sourceCombinedState.getOutgoingTransitions().add(nextTransition);
						stateNameToStateMap.get(nextStateName).getIncomingTransitions().add(nextTransition);
					}else{
						nextState.setName(nextStateName);
						List<State> combinedTargetStates = new ArrayList<State>();
						combinedTargetStates.add(firstContainingTargetState);
						combinedTargetStates.add(secondContainingTargetState);
						newStateCombinesStates.put(nextState, combinedTargetStates);
						
						nextTransition.setToState(nextState);
						sourceCombinedState.getOutgoingTransitions().add(nextTransition);
						nextState.getIncomingTransitions().add(nextTransition);
						
						stateNameToStateMap.put(nextStateName, nextState);
						
						//only in this case add next state to open
						composition.getStates().add(nextState);
						open.push(nextState);
					}
				}else{
					//disjunct event
					State nextState = LtsFactory.eINSTANCE.createState();
					State firstContainingTargetState = LtsFactory.eINSTANCE.createState();
					State secondContainingTargetState = LtsFactory.eINSTANCE.createState();
					Transition nextTransition = LtsFactory.eINSTANCE.createTransition();
					
					nextTransition.setName(t.getName());
					nextTransition.setFromState(sourceCombinedState);
					nextTransition.setToState(nextState);
					composition.getTransitions().add(nextTransition);
					firstContainingTargetState = t.getToState();
					
					
					secondContainingTargetState = secondContainingState;
					
					
					String nextStateName = firstContainingTargetState.getName() + ", " + secondContainingTargetState.getName();
					//if state is already explored
					if(stateNameToStateMap.containsKey(nextStateName)){
						nextTransition.setToState(stateNameToStateMap.get(nextStateName));
						sourceCombinedState.getOutgoingTransitions().add(nextTransition);
						stateNameToStateMap.get(nextStateName).getIncomingTransitions().add(nextTransition);
					}else{
						nextState.setName(nextStateName);
						List<State> combinedTargetStates = new ArrayList<State>();
						combinedTargetStates.add(firstContainingTargetState);
						combinedTargetStates.add(secondContainingTargetState);
						newStateCombinesStates.put(nextState, combinedTargetStates);
						
						nextTransition.setToState(nextState);
						sourceCombinedState.getOutgoingTransitions().add(nextTransition);
						nextState.getIncomingTransitions().add(nextTransition);
						
						stateNameToStateMap.put(nextStateName, nextState);
						
						//only in this case add next state to open
						composition.getStates().add(nextState);
						open.push(nextState);
					}
				}
			}
			for(Transition t2 : secondContainingState.getOutgoingTransitions()){
				if(lts1.getAlphabet().hasElement(t2.getName())){
				// do nothing because it's already done
				}else{
					//disjunct event
					State nextState = LtsFactory.eINSTANCE.createState();
					State firstContainingTargetState = LtsFactory.eINSTANCE.createState();
					State secondContainingTargetState = LtsFactory.eINSTANCE.createState();
					Transition nextTransition = LtsFactory.eINSTANCE.createTransition();
					
					nextTransition.setName(t2.getName());
					nextTransition.setFromState(sourceCombinedState);
					secondContainingTargetState = t2.getToState();
					
					firstContainingTargetState = firstContainingState;
					
					String nextStateName = firstContainingTargetState.getName() + ", " + secondContainingTargetState.getName();
					//if state is already explored
					if(stateNameToStateMap.containsKey(nextStateName)){
						nextTransition.setToState(stateNameToStateMap.get(nextStateName));
						sourceCombinedState.getOutgoingTransitions().add(nextTransition);
						stateNameToStateMap.get(nextStateName).getIncomingTransitions().add(nextTransition);
					}else{
						nextState.setName(nextStateName);
						List<State> combinedTargetStates = new ArrayList<State>();
						combinedTargetStates.add(firstContainingTargetState);
						combinedTargetStates.add(secondContainingTargetState);
						newStateCombinesStates.put(nextState, combinedTargetStates);
						
						nextTransition.setToState(nextState);
						sourceCombinedState.getOutgoingTransitions().add(nextTransition);
						nextState.getIncomingTransitions().add(nextTransition);
						
						stateNameToStateMap.put(nextStateName, nextState);
						
						//only in this case add next state to open
						composition.getStates().add(nextState);
						open.push(nextState);
					}
				}
			}
		}
		return composition;
	}
}
