package de.luh.se.fmse.manager;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.luh.se.fmse.core.ba.composition.BAComposition;
import de.luh.se.fmse.core.helper.NestedDFSHelper;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.ui.views.CompositeLTSView;
import de.luh.se.fmse.ui.views.FirstLTSView;
import de.luh.se.fmse.ui.views.SecondLTSView;

public class InputManager implements IObjectActionDelegate{
	
	// perform composition on BA
	public void performParallelComposition(LTS lts1, LTS lts2){
	
		BAComposition ba = new BAComposition(); 
		LTS composedBA= ba.performBAComposition(lts1, lts2);
		
		
//		ParallelComposition core = new ParallelComposition();
//		
//		LTS composedLTS = core.performParallelComposition(lts1, lts2);
//		
		IViewPart view = getView("de.luh.se.fmse.ui.views.CompositeLTSView");
		((CompositeLTSView) view.getViewSite().getPart()).setInput(composedBA.getStates());
		
		NestedDFSHelper myNest = new NestedDFSHelper();
		myNest.performNestedDFS(composedBA);
		
		MessageBox dialog = new MessageBox(Display.getDefault().getActiveShell(), SWT.OK);
		dialog.setText("Product Construction for BA");
		dialog.setMessage("Es wurde " + (myNest.hasCircle() ? "ein " : "kein ") + "Zirkel gefunden. " 
				+"\n" + (myNest.hasCircle() ? "Die Bedingung ist nicht erf�llt. " : "Die Bedingung ist erf�llt. "));

		dialog.open(); 
	}
	
	
	
	@Override
	public void run(IAction action) {		
		
		LTS lts1 = null;
		LTS lts2 = null;
		
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
		final IFile file = (IFile) structuredSelection.toList().get(0);

		final ResourceSet resourceSet = new ResourceSetImpl();
		final Resource ltsResource = resourceSet.getResource(URI.createPlatformResourceURI(file.getFullPath().toString(), true), true);
			
		if(ltsResource.getContents().get(0) instanceof LTS){
			lts1 = (LTS) ltsResource.getContents().get(0);
		}else{
		
		}
		
		final IFile file2 = (IFile) structuredSelection.toList().get(1);
		final ResourceSet resourceSet2 = new ResourceSetImpl();
		final Resource ltsResource2 = resourceSet2.getResource(URI.createPlatformResourceURI(file2.getFullPath().toString(), true), true);
		
		if(ltsResource2.getContents().get(0) instanceof LTS){
			lts2 = (LTS) ltsResource2.getContents().get(0);
		}
		
		if(lts1 != null){
			IViewPart view = getView("de.luh.se.fmse.ui.views.FirstLTSView");
			((FirstLTSView) view.getViewSite().getPart()).setInput(lts1.getStates());
		}
		if(lts2 != null){
			IViewPart view2 = getView("de.luh.se.fmse.ui.views.SecondLTSView");
			((SecondLTSView) view2.getViewSite().getPart()).setInput(lts2.getStates());
		}
		
		if(lts2 != null){
			performParallelComposition(lts1, lts2);
		}else{
			NestedDFSHelper helper = new NestedDFSHelper();
			helper.performNestedDFS(lts1);
		}
		
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub
		
	}
	
	public static IViewPart getView(String id) {
		IViewReference viewReferences[] = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if (id.equals(viewReferences[i].getId())) {
				return viewReferences[i].getView(false);
			}
		}
		return null;
	}

}
