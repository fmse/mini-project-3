/**
 */
package de.luh.se.fmse.ast;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OR Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.ast.ORNode#getLeftChild <em>Left Child</em>}</li>
 *   <li>{@link de.luh.se.fmse.ast.ORNode#getRightChild <em>Right Child</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.ast.AstPackage#getORNode()
 * @model
 * @generated
 */
public interface ORNode extends Node {
	/**
	 * Returns the value of the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Child</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Child</em>' containment reference.
	 * @see #setLeftChild(Node)
	 * @see de.luh.se.fmse.ast.AstPackage#getORNode_LeftChild()
	 * @model containment="true"
	 * @generated
	 */
	Node getLeftChild();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.ast.ORNode#getLeftChild <em>Left Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Child</em>' containment reference.
	 * @see #getLeftChild()
	 * @generated
	 */
	void setLeftChild(Node value);

	/**
	 * Returns the value of the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Child</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Child</em>' containment reference.
	 * @see #setRightChild(Node)
	 * @see de.luh.se.fmse.ast.AstPackage#getORNode_RightChild()
	 * @model containment="true"
	 * @generated
	 */
	Node getRightChild();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.ast.ORNode#getRightChild <em>Right Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Child</em>' containment reference.
	 * @see #getRightChild()
	 * @generated
	 */
	void setRightChild(Node value);

} // ORNode
