/**
 */
package de.luh.se.fmse.ast.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.luh.se.fmse.ast.AstPackage;
import de.luh.se.fmse.ast.Node;
import de.luh.se.fmse.ast.ORNode;
import de.luh.se.fmse.lts.LTS;
import de.luh.se.fmse.lts.State;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OR Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.luh.se.fmse.ast.impl.ORNodeImpl#getLeftChild <em>Left Child</em>}</li>
 *   <li>{@link de.luh.se.fmse.ast.impl.ORNodeImpl#getRightChild <em>Right Child</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ORNodeImpl extends NodeImpl implements ORNode {
	/**
	 * The cached value of the '{@link #getLeftChild() <em>Left Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftChild()
	 * @generated
	 * @ordered
	 */
	protected Node leftChild;

	/**
	 * The cached value of the '{@link #getRightChild() <em>Right Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightChild()
	 * @generated
	 * @ordered
	 */
	protected Node rightChild;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ORNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AstPackage.eINSTANCE.getORNode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getLeftChild() {
		return leftChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftChild(Node newLeftChild, NotificationChain msgs) {
		Node oldLeftChild = leftChild;
		leftChild = newLeftChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AstPackage.OR_NODE__LEFT_CHILD, oldLeftChild, newLeftChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftChild(Node newLeftChild) {
		if (newLeftChild != leftChild) {
			NotificationChain msgs = null;
			if (leftChild != null)
				msgs = ((InternalEObject)leftChild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AstPackage.OR_NODE__LEFT_CHILD, null, msgs);
			if (newLeftChild != null)
				msgs = ((InternalEObject)newLeftChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AstPackage.OR_NODE__LEFT_CHILD, null, msgs);
			msgs = basicSetLeftChild(newLeftChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AstPackage.OR_NODE__LEFT_CHILD, newLeftChild, newLeftChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getRightChild() {
		return rightChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightChild(Node newRightChild, NotificationChain msgs) {
		Node oldRightChild = rightChild;
		rightChild = newRightChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AstPackage.OR_NODE__RIGHT_CHILD, oldRightChild, newRightChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightChild(Node newRightChild) {
		if (newRightChild != rightChild) {
			NotificationChain msgs = null;
			if (rightChild != null)
				msgs = ((InternalEObject)rightChild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AstPackage.OR_NODE__RIGHT_CHILD, null, msgs);
			if (newRightChild != null)
				msgs = ((InternalEObject)newRightChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AstPackage.OR_NODE__RIGHT_CHILD, null, msgs);
			msgs = basicSetRightChild(newRightChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AstPackage.OR_NODE__RIGHT_CHILD, newRightChild, newRightChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AstPackage.OR_NODE__LEFT_CHILD:
				return basicSetLeftChild(null, msgs);
			case AstPackage.OR_NODE__RIGHT_CHILD:
				return basicSetRightChild(null, msgs);
		}
		return eDynamicInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AstPackage.OR_NODE__LEFT_CHILD:
				return getLeftChild();
			case AstPackage.OR_NODE__RIGHT_CHILD:
				return getRightChild();
		}
		return eDynamicGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AstPackage.OR_NODE__LEFT_CHILD:
				setLeftChild((Node)newValue);
				return;
			case AstPackage.OR_NODE__RIGHT_CHILD:
				setRightChild((Node)newValue);
				return;
		}
		eDynamicSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AstPackage.OR_NODE__LEFT_CHILD:
				setLeftChild((Node)null);
				return;
			case AstPackage.OR_NODE__RIGHT_CHILD:
				setRightChild((Node)null);
				return;
		}
		eDynamicUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AstPackage.OR_NODE__LEFT_CHILD:
				return leftChild != null;
			case AstPackage.OR_NODE__RIGHT_CHILD:
				return rightChild != null;
		}
		return eDynamicIsSet(featureID);
	}
	
	/**
	 * @generated NOT
	 */
	@Override
	public EList<State> validate(LTS lts) {
		EList<State> resultStates = new UniqueEList<State>();
		
		if (this.leftChild == null || this.rightChild == null)
			return resultStates;
		
		EList<State> leftChildResult = this.leftChild.validate(lts);
		EList<State> rightChildResult = this.rightChild.validate(lts);
		
		resultStates.addAll(rightChildResult);
		resultStates.addAll(leftChildResult);
		
		return resultStates;
	}
	
	/**
	 * @generated NOT
	 */
	public String toString() {
		return "(" + this.leftChild.toString() + ") OR (" + this.rightChild.toString() + ")";
	}

} //ORNodeImpl
