/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.State#getName <em>Name</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#getIncomingTransitions <em>Incoming Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#getOutgoingTransitions <em>Outgoing Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#getAtomicPrep <em>Atomic Prep</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#getSatisfiedNodes <em>Satisfied Nodes</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#isSatisfiedBlue <em>Satisfied Blue</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#isSatisfiedRed <em>Satisfied Red</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.State#isLTSsatisfied <em>LT Ssatisfied</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getState()
 * @model
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.State#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Incoming Transitions</b></em>' reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Transitions</em>' reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_IncomingTransitions()
	 * @model
	 * @generated
	 */
	EList<Transition> getIncomingTransitions();

	/**
	 * Returns the value of the '<em><b>Outgoing Transitions</b></em>' reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing Transitions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing Transitions</em>' reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_OutgoingTransitions()
	 * @model
	 * @generated
	 */
	EList<Transition> getOutgoingTransitions();

	/**
	 * Returns the value of the '<em><b>Atomic Prep</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Prep</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Prep</em>' attribute list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_AtomicPrep()
	 * @model
	 * @generated
	 */
	EList<String> getAtomicPrep();

	/**
	 * Returns the value of the '<em><b>Satisfied Nodes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Satisfied Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfied Nodes</em>' attribute list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_SatisfiedNodes()
	 * @model
	 * @generated
	 */
	EList<String> getSatisfiedNodes();

	/**
	 * Returns the value of the '<em><b>Satisfied Blue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Satisfied Blue</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfied Blue</em>' attribute.
	 * @see #setSatisfiedBlue(boolean)
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_SatisfiedBlue()
	 * @model
	 * @generated
	 */
	boolean isSatisfiedBlue();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.State#isSatisfiedBlue <em>Satisfied Blue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Satisfied Blue</em>' attribute.
	 * @see #isSatisfiedBlue()
	 * @generated
	 */
	void setSatisfiedBlue(boolean value);

	/**
	 * Returns the value of the '<em><b>Satisfied Red</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Satisfied Red</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfied Red</em>' attribute.
	 * @see #setSatisfiedRed(boolean)
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_SatisfiedRed()
	 * @model
	 * @generated
	 */
	boolean isSatisfiedRed();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.State#isSatisfiedRed <em>Satisfied Red</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Satisfied Red</em>' attribute.
	 * @see #isSatisfiedRed()
	 * @generated
	 */
	void setSatisfiedRed(boolean value);

	/**
	 * Returns the value of the '<em><b>LT Ssatisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>LT Ssatisfied</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>LT Ssatisfied</em>' attribute.
	 * @see #setLTSsatisfied(boolean)
	 * @see de.luh.se.fmse.lts.LtsPackage#getState_LTSsatisfied()
	 * @model
	 * @generated
	 */
	boolean isLTSsatisfied();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.State#isLTSsatisfied <em>LT Ssatisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>LT Ssatisfied</em>' attribute.
	 * @see #isLTSsatisfied()
	 * @generated
	 */
	void setLTSsatisfied(boolean value);

} // State
