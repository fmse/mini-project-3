/**
 */
package de.luh.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getStates <em>States</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getStartStates <em>Start States</em>}</li>
 *   <li>{@link de.luh.se.fmse.lts.LTS#getFinalState <em>Final State</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.luh.se.fmse.lts.LtsPackage#getLTS()
 * @model
 * @generated
 */
public interface LTS extends EObject {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Alphabet</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alphabet</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alphabet</em>' containment reference.
	 * @see #setAlphabet(Alphabet)
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_Alphabet()
	 * @model containment="true"
	 * @generated
	 */
	Alphabet getAlphabet();

	/**
	 * Sets the value of the '{@link de.luh.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alphabet</em>' containment reference.
	 * @see #getAlphabet()
	 * @generated
	 */
	void setAlphabet(Alphabet value);

	/**
	 * Returns the value of the '<em><b>Start States</b></em>' reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start States</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start States</em>' reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_StartStates()
	 * @model
	 * @generated
	 */
	EList<State> getStartStates();

	/**
	 * Returns the value of the '<em><b>Final State</b></em>' reference list.
	 * The list contents are of type {@link de.luh.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final State</em>' reference list.
	 * @see de.luh.se.fmse.lts.LtsPackage#getLTS_FinalState()
	 * @model
	 * @generated
	 */
	EList<State> getFinalState();

} // LTS
